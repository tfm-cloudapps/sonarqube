# SonarQube

[SonarQube](https://docs.sonarqube.org/latest/) is an open source quality management platform that analyzes and measures code's technical quality. It enables developers to detect code issues, vulnerabilities, and bugs in early stages.